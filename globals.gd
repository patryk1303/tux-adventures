extends Node

const PLAYER = {
	move_forward = "move_forward",
	move_backward = "move_backward",
	move_left = "move_left",
	move_right = "move_right",
	move_jump = "move_jump",
	move_duck = "move_duck",
	move_attack = "move_attack",
	state_idle = "idle",
	state_run = "run",
	anim_main = "Main_Anim",
}

const SHOW_UI = "show_ui"

const PREV = 'previous'
const NEXT = 'next'

const UP = Vector3(0, 1, 0)

const GROUP_TILES = "tiles"
const GROUP_PLAYER = "player"
const GROUP_CRATES = "crates"
const GROUP_RAYS_UP = "up-rays"

const GRAVITY = -9.8