extends Node

export var herrings = 0 setget set_herrings, get_herrings
export var lives = 3 setget set_lives, get_lives

#
# HERRINGS
#

func set_herrings(amount):
	if amount > 99:
		herrings -= 100
		self.lives += 1
	else:
		herrings = amount

func get_herrings():
	return herrings

#
# LIVES
#

func set_lives(amount):
	if amount <= 99:
		lives = amount

func get_lives():
	return lives