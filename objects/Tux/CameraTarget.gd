extends Spatial

export(NodePath) var character_path
export(float, 0.1, 2, 0.1) var camera_speed = 0.5
export(float, 0.1, 2, 0.01) var camera_height = 0.66

export(Vector2) var max_x = Vector2(-100, 100)
export(Vector2) var max_y = Vector2(-100, 100)
export(Vector2) var max_z = Vector2(-100, 100)

var character
var new_camera_rotate = Vector3()

func _ready():
	$MeshInstance.hide()
	character = get_node(character_path)

	new_camera_rotate = rotation

func _physics_process(delta):
	var target_position = character.translation
	var current_position = translation

	var target_pos = Vector3()
	target_pos.x = clamp(target_position.x, max_x.x, max_x.y)
	target_pos.y = clamp(target_position.y + camera_height, max_y.x, max_y.y)
	target_pos.z = clamp(target_position.z, max_z.x, max_z.y)

	translation = target_pos.linear_interpolate(target_pos, camera_speed)

	var look_pos = character.get_transform().origin

	$Camera.look_at(look_pos, globals.UP)

	if Input.is_key_pressed(KEY_O):
		rotation.y += deg2rad(90)
	if Input.is_key_pressed(KEY_P):
		rotation.y -= deg2rad(90)

	var current_rotate = rotation
	var new_rotate = Vector3()

	new_rotate.x = lerp(current_rotate.x, new_camera_rotate.x, 0.1)
	new_rotate.y = lerp(current_rotate.y, new_camera_rotate.y, 0.1)
	new_rotate.z = lerp(current_rotate.z, new_camera_rotate.z, 0.1)

	rotation = new_rotate
