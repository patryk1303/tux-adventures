extends KinematicBody

export(int, 1, 20, 1) var speed = 7
export(int, 1, 20, 1) var slide_speed = 9
export(int, 1, 20, 1) var acceleration = 3
export(int, 1, 20, 1) var de_acceleration = 5
export(int, 1, 20, 1) var jump_force = 5

var camera
var cam_transform
var alive = true

export(NodePath) var camera_path

var key_map = {
	move_forward = false,
	move_backward = false,
	move_left = false,
	move_right = false,
	move_jump = false,
	move_attack = false,
	move_slide = false
}

var current_blends = ["Idle_Run","Run_Jump","Jump_Fall"]

var velocity = Vector3()
var hv = Vector3()
var new_dir = Vector3()

var anim = "Idle_Run"
var new_anim = ""
var anim_blend = 0
var anim_blend_dest = 1

onready var attack_timer = $Timers/Attack

onready var rays = [
	$DownRay1,
	$DownRay2,
	$DownRay3,
	$DownRay4,
	$HorRay1,
	$HorRay2,
	$HorRay3,
	$HorRay4,
	$HorRay5,
	$HorRay6,
	$HorRay7,
	$HorRay8,
	$UpRay1,
	$UpRay2,
	$UpRay3,
	$UpRay4
]

func _ready():
	camera = get_node(String(camera_path) + "/Camera")
	alive = true

func _input(event):
	for key_name in key_map:
		if event.is_action_pressed(key_name) and alive:
			key_map[key_name] = true
		elif event.is_action_released(key_name):
			key_map[key_name] = false

func _process(delta):
	for current_blend in current_blends:
		var blend_val = $AnimTree.blend2_node_get_amount(current_blend)
		blend_val = lerp(blend_val, 0, 0.1)
		$AnimTree.blend2_node_set_amount(current_blend, blend_val)

func _physics_process(delta):
	handle_movement(delta)
	handle_animation(delta)

	for ray in rays:
		check_ray_with_crate(ray)

	if Input.is_key_pressed(KEY_R):
		get_tree().reload_current_scene()

func handle_movement(delta):
	var dir = Vector3()
	var is_moving = get_is_moving()

	cam_transform = camera.get_global_transform()

#	print(cam_transform)

	var mult = 1

	if get_is_sliding():
		mult = 0

	if key_map.move_forward:
		dir -= cam_transform.basis[2] * mult
	if key_map.move_backward:
		dir += cam_transform.basis[2] * mult
	if key_map.move_left:
		dir -= cam_transform.basis[0] * mult
	if key_map.move_right:
		dir += cam_transform.basis[0] * mult
	if (key_map.move_attack
		and !get_is_attacking()
		and $Timers/ActionSustain.time_left == 0
		and !get_is_sliding()):
		attack()
	if (key_map.move_slide
		and !get_is_sliding()
		and !get_is_attacking()
		and is_on_floor()
		and is_moving
		and $Timers/ActionSustain.time_left == 0):
			slide()

	var is_sliding = get_is_sliding()

	dir.y = 0
	dir = dir.normalized()

	if !is_sliding:
		new_dir = dir

	if alive:
		velocity.y += delta * globals.GRAVITY
	else:
		velocity.y = 0

	hv = velocity
	hv.y = 0

	var speed_to_use = speed

	if is_sliding:
		speed_to_use = slide_speed

	var new_pos = new_dir * speed_to_use
	var accel = de_acceleration

	if dir.dot(hv) > 0:
		accel = acceleration

	hv = hv.linear_interpolate(new_pos, accel * delta)

	velocity.x = hv.x
	velocity.z = hv.z

	velocity = move_and_slide(velocity, globals.UP, 0.15)

	if is_on_floor() and key_map.move_jump:
		jump()

	if is_moving:
		var angle = atan2(hv.x, hv.z)
		var char_rot = self.get_rotation()

		char_rot.y = angle
		self.set_rotation(char_rot)

func handle_animation(delta):
	var is_jumping = get_is_jumping()
	var is_falling = get_is_falling()
	var is_moving = get_is_moving()

	if !is_jumping and !is_falling:
		var timescale = hv.length() / speed * 2

		timescale = clamp(timescale, 0.5, 4)

		$AnimTree.timescale_node_set_scale("scale", timescale)

		new_anim = "Idle_Run"
		if is_moving:
			anim_blend_dest = 1
		else:
			anim_blend_dest = 0

	if is_jumping:
		anim_blend_dest = 1
		new_anim = "Run_Jump"
	elif is_falling:
		anim_blend_dest = 1
		new_anim = "Jump_Fall"

	if new_anim != anim:
		anim = new_anim
		anim_blend = 0

	anim_blend = lerp(anim_blend, anim_blend_dest, 0.15)
	$AnimTree.blend2_node_set_amount(anim, anim_blend)

func jump():
	velocity.y = jump_force

	if get_is_sliding():
		velocity.y *= 1.2
#		$Timers/SlideTimer.stop()

	$Jump.play()
	$Timers/JumpTimer.start()

func de_jump():
	velocity.y = -jump_force / 3

func attack():
	$Timers/Attack.start()
	$AnimTree.transition_node_set_current(globals.PLAYER.anim_main, 1)

func slide():
	$Timers/SlideTimer.start()
	$AnimTree.transition_node_set_current(globals.PLAYER.anim_main, 2)
	$StandardCollision.disabled = true
	$SlideCollision.disabled = false

func die():
	alive = false
	$Timers/DieTimer.start()

func check_ray_with_crate(ray):
	if ray.is_colliding():
		var collider = ray.get_collider()
		if collider and collider.is_in_group("crates"):
			collider.handle_player_collision(self, ray)

func get_is_moving():
	return key_map.move_forward or key_map.move_backward or key_map.move_left or key_map.move_right

func get_is_jumping():
	return !is_on_floor() and velocity.y > 0

func get_is_falling():
	return !is_on_floor() and velocity.y < 0

func get_is_attacking():
	return $Timers/Attack.time_left > 0

func get_is_sliding():
	return $Timers/SlideTimer.time_left > 0

func _on_Attack_timeout():
	$AnimTree.transition_node_set_current(globals.PLAYER.anim_main, 0)
	$Timers/ActionSustain.start()

func _on_JumpTimer_timeout():
	$Jump.stop()

func _on_SlideTimer_timeout():
	$AnimTree.transition_node_set_current(globals.PLAYER.anim_main, 0)
	$StandardCollision.disabled = false
	$SlideCollision.disabled = true
	$Timers/ActionSustain.start()

func _on_DieTimer_timeout():
	game.lives -= 1
	get_tree().reload_current_scene()
