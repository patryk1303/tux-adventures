extends KinematicBody

signal crate_broken

var tux_entered = false
var tux

var crate_destoyed = false

var velocity = Vector3()

export(bool) var affected_by_gravity = true
export(PackedScene) var item_inside = null
export(int, 1, 15, 1) var item_amount = 1
export(bool) var countable = true

func _physics_process(delta):

	if crate_destoyed:
		hide()
		$CollisionShape.disabled = true

	velocity.x = 0
	velocity.z = 0
	if affected_by_gravity:
		velocity.y += delta * globals.GRAVITY * 0.8
	else:
		velocity.y = 0

	velocity = move_and_slide(velocity, globals.UP)

func check_ray_collision(ray):
	var collider = ray.get_collider()

	if collider and collider.is_in_group("player"):
		tux = collider
		handle_player_collision(tux)

func handle_player_collision(player, ray):
	var is_attacking = player.get_is_attacking()
	var is_jumping = player.get_is_jumping()
	var is_falling = player.get_is_falling()
	var is_sliding = player.get_is_sliding()
	var should_be_deleted = false

	if crate_destoyed:
		return

	if get_is_falling() and ray.is_in_group("up-rays"):
		should_be_deleted = true
	elif is_falling:
		if (is_attacking
			or is_sliding
			or player.get_node("Timers/ActionSustain").time_left > 0):
			pass
		else:
			if !ray.is_in_group("up-rays"):
				player.jump()
			should_be_deleted = true
	elif is_jumping and ray.is_in_group("up-rays"):
		player.de_jump()
	elif is_attacking:
		should_be_deleted = true
	elif is_sliding:
		should_be_deleted = true

	if should_be_deleted:
		destroy_crate()

func destroy_crate():
	crate_destoyed = true

	$DestroyParticles.emitting = true

	if !$CrateBroken.playing:
		$CrateBroken.play()
		$CrateDestroy.start()

	emit_signal("crate_broken")

	if item_inside:
		for i in range(item_amount):
			var item = item_inside.instance()
			item.translation = translation + Vector3(rand_range(0, 0.2), -0.2, rand_range(0, 0.2))
			item.rotation = Vector3(0, deg2rad(rand_range(0, 360)), 0)
			get_tree().get_root().add_child(item)

func get_is_falling():
	return affected_by_gravity and !is_on_floor() and velocity.y < 0

func _on_CrateDestroy_timeout():
	$CrateBroken.stop()
	queue_free()
