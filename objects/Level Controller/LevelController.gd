extends Node

export(int, 0, 999, 1) var crates_count = 10
export(AudioStream) var level_music = null
export(NodePath) var crates_spatial_node_path = null

var crates_node
var broken_boxes = 0

func _ready():
	broken_boxes = 0

	if level_music:
		$AudioStreamPlayer.stream = level_music
		$AudioStreamPlayer.play()

	if crates_spatial_node_path:
		crates_node = get_node(crates_spatial_node_path)
		var crates = crates_node.get_children()
		for crate in crates:
			if crate.countable:
				crate.connect("crate_broken", self, "_on_crate_broken")

func _process(delta):
	pass
func _on_crate_broken():
	broken_boxes += 1