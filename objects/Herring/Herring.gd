extends Area

var can_collect = false

func _on_Herring_body_entered(body):
	if body.is_in_group("player") and can_collect:
		if body.attack_timer.time_left > 0:
			pass
			# Play sound of broken herring
		else:
			game.herrings += 1
			# Play sound of collected herring
		queue_free()


func _on_StartTimer_timeout():
	can_collect = true
