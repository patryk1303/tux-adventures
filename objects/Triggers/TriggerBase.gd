extends Area

export(NodePath) var target_path

var target_node

func _ready():
	$MeshInstance.hide()

	target_node = get_node(target_path)

func _on_TriggerBase_body_entered(body):
	pass # replace with function body


func _on_TriggerBase_body_exited(body):
	pass # replace with function body
