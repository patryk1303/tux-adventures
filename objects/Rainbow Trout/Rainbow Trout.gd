extends Area

var can_collect = false

func _on_Rainbow_Trout_body_entered(body):
	if body.is_in_group("player") and can_collect:
		if body.attack_timer.time_left > 0:
			pass
			# Play sound of broken herring
		else:
			game.herrings += 1
			$CollectSound.play()
			# Play sound of collected herring
		$CollectTimer.start()

func _on_StartTimer_timeout():
	can_collect = true

func _on_CollectTimer_timeout():
	can_collect = false
	queue_free()
