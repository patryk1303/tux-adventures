extends Node2D

export(NodePath) var level_controller_path = null

var level_controller

func _ready():
	if level_controller_path:
		level_controller = get_node(level_controller_path)

func _process(delta):
	$Herrings/HerringCount.text = String(game.herrings)
	$Lives/LivesCount.text = String(game.lives)

	if level_controller:
		$Crates/CratesCount.text = String(level_controller.broken_boxes) + " / " + String(level_controller.crates_count)
