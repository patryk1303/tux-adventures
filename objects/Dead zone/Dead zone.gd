extends Area

func _ready():
	$StaticBody/MeshInstance.hide()
	pass

func _on_Dead_zone_body_entered(body):
	if body.is_in_group(globals.GROUP_PLAYER):
		body.die()
